<?php

namespace App\Http\Controllers;

use App\Http\Resources\Job\JobCollection;
use App\Models\Job;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\JobRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Repositories\src\JobRepository as JobRepository;

class JobController extends Controller
{
    private $job;
    public function __construct(JobRepository $repository)
    {
        $this->job = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return string
     */
    public function index(Request $request)
    {
        try {
            $pageSize = 10;
            if((int)($request->input('pageSize')) != null) {
                $pageSize = (int)($request->input('pageSize'));
            }
            return new JobCollection(Job::paginate($pageSize));
        }
        catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param JobRequest $request
     * @return JsonResponse
     */
    public function store(JobRequest $request)
    {
        try {
            $job = new Job;
            $job->name = $request->post('name');
            $job->description = $request->post('description');
            //$job->status = $request->post('status');
            $job->fk_job_types_id = $request->post('job_types_id');
            $job->start_date = $request->post('start_date');
            $job->end_date = $request->post('end_date');
            $job->status = 0;
            $this->job->save($job);
            return response()->json(['data' => $job, 'success' => true], 201);
        }
        catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Job $job
     * @return JsonResponse
     */
    public function show(Job $job)
    {
        try {
            $result = $this->job->find($job->id);
            return response()->json(['data' => $result, 'success' => true], 200);
        }
        catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Job $job
     * @return void
     */
    public function edit(Job $job)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JobRequest $request
     * @param Job $job
     * @return JsonResponse
     */
    public function update(JobRequest $request, Job $job)
    {
        try {
            $value = [
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'fk_job_type_id' => $request->input('type')
            ];
            $this->job->update($job, $request->input('job'));
            return response()->json(['data' => $job, 'request' => $request->all(), 'success' => true], 201);
        }
        catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Job $job
     * @return JsonResponse
     */
    public function destroy(Job $job)
    {
        try {
            $this->job->delete($job);
            return \response()->json(['success' => true, 'data' => $job], 200);
        }
        catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 401);
        }
    }
}
