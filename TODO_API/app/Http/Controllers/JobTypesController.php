<?php

namespace App\Http\Controllers;

use App\Models\JobTypes;
use Illuminate\Http\Request;
use App\Repositories\src\JobTypesRepository as JobTypesRepository;
use App\Http\Resources\JobTypes\JobTypesCollection;

class JobTypesController extends Controller
{
    private $jobtype;
    public function __construct(JobTypesRepository $repository) // inject
    {
        $this->jobtype = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new JobTypesCollection($this->jobtype->all());
        //return JobTypes::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobTypes  $jobTypes
     * @return \Illuminate\Http\Response
     */
    public function show(JobTypes $jobTypes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobTypes  $jobTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(JobTypes $jobTypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobTypes  $jobTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobTypes $jobTypes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobTypes  $jobTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobTypes $jobTypes)
    {
        //
    }
}
