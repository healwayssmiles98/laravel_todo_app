<?php

namespace App\Http\Resources\JobTypes;

use Illuminate\Http\Resources\Json\ResourceCollection;

class JobTypesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => [
                'self' => 'link-value'
            ]
        ];
    }
}
