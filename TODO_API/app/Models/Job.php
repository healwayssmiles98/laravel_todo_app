<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\JobTypes;
use App\Models\JobDetails;

class Job extends Model
{
    //
    protected $fillable = [
        'name',
        'description',
        'start_date',
        'end_date',
        'created_by',
        'updated_by',
        'status',
        'job_types_id'
    ];

    public function type () {
        return $this->hasOne(JobTypes::class, 'job_types_id');
    }
    public function detail () {
        return $this->hasOne(JobDetails::class, 'fk_jobs_id');
    }
}
