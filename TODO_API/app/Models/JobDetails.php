<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Job;

class JobDetails extends Model
{
    //
    protected $fillable = [
      'quality', 'rating', 'star', 'fk_jobs_id'
    ];

    public function job () {
        return $this->belongsTo(Job::class, 'fk_jobs_id');
    }
}
