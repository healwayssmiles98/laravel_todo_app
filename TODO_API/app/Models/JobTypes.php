<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Job;
use Illuminate\Support\Facades\DB;

class JobTypes extends Model
{
    //
    protected $fillable = [
        'job_type_name', 'description'
    ];

    public function jobs () {
        return $this->hasMany(Job::class, 'job_types_id');
    }
}
