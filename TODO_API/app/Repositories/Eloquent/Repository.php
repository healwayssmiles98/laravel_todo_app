<?php


namespace App\Repositories\Eloquent;
use App\Repositories\Interfaces\RepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class Repository implements RepositoryInterface
{

    private $app;

    protected $model;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();

    /**
     * @return Model
     * @throws Exception
     */
    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }

    public function all($columns = array('*')) {
        return $this->model->get($columns);
    }

    public function index()
    {
        // TODO: Implement index() method.
       return $this->model::all();
    }

    public function paginate(int $pageSize)
    {
        // TODO: Implement paginate() method.
    }

    public function save(object $object)
    {
        // TODO: Implement save() method.
        return $object->save();
    }

    public function update(object $object, array $data)
    {
        // TODO: Implement update() method.
        return $object->update($data);
    }

    public function delete(object $object)
    {
        // TODO: Implement delete() method.
        return $object->delete();
    }

    public function find(int $id)
    {
        // TODO: Implement find() method.
        return $this->model::all()->find($id);
    }

    public function findBy($field, $value, $columns = array('*'))
    {
        // TODO: Implement findBy() method.
    }
}
