<?php
    namespace App\Repositories\Interfaces;

    interface RepositoryInterface {

        public function all($columns = array('*'));

        public function index();

        public function paginate(int $pageSize);

        public function save(object $object);

        public function update(object $object, array $data);

        public function delete(object $object);

        public function find(int $id);

        public function findBy($field, $value, $columns = array('*'));
    }

?>
