<?php


namespace App\Repositories\src;
use App\Models\JobTypes;
use App\Repositories\Eloquent\Repository;

class JobTypesRepository extends Repository
{

    /**
     * @inheritDoc
     */
    function model()
    {
        // TODO: Implement model() method.
        return JobTypes::class;
    }
}
