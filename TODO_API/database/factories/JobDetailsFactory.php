<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\JobDetails;
use App\Models\Job;
use Faker\Generator as Faker;

$factory->define(JobDetails::class, function (Faker $faker) {
    return [
        'quality' => $faker->state,
        'rating' => $faker->text,
        'star' => $faker->numberBetween(0,5),
        'fk_jobs_id' => $faker->numberBetween(0, 100)
    ];
});
