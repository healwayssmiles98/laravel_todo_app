<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Job;
use App\Models\JobTypes;
use Faker\Generator as Faker;

$factory->define(Job::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'start_date' => $faker->dateTime,
        'end_date' => $faker->dateTime,
        'created_by' => $faker->name,
        'updated_by' => $faker->name,
        'status' => $faker->numberBetween(0, 3),
        'fk_job_types_id' => $faker->numberBetween(0, 50)
    ];
});
