<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\JobTypes;
use Faker\Generator as Faker;

$factory->define(JobTypes::class, function (Faker $faker) {
    return [
        'job_type_name' => $faker->text,
        'description' => $faker->text,
    ];
});
