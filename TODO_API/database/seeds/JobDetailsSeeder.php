<?php

use Illuminate\Database\Seeder;

class JobDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\JobDetails::class, 100)->create();
    }
}
