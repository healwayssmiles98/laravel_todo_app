<?php


namespace App\Http\Common\ApisUrl;


class ApisUrl
{
    // token
    public const REQUEST_TOKEN = "http://localhost:8888/oauth/token";

    // job api
    public const JOB_API = "http://localhost:8888/api/job/";

    // job type api
    public const JOB_TYPES_API = "http://localhost:8888/api/types/";
}
