<?php
namespace App\Http\Common\Base;
use App\Http\Common\ApisUrl\ApisUrl;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\DB;
use \Illuminate\Http\Request;

trait UserToken {

    // tạo token
    public function createRequestToken (Request $request)
    {
        try {
            $header = [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ];
            $client = new GuzzleClient([
                'headers' => $header
            ]);

            $clientInfo = DB::table('oauth_clients')->find(2);
            $response = $client->post(ApisUrl::REQUEST_TOKEN, [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => $clientInfo->secret,
                    'username' => $request->get('email'),
                    'password' => $request->get('password')
                ]
            ]);
            return json_decode((string) $response->getBody(), true);
        }
        catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
?>
