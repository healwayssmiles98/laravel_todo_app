<?php
namespace App\Http\Common\Base;
use GuzzleHttp\Client;

trait HttpTrait {
    public function httpClient (string $token, string $method, string $url, string $body = '{}') {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token
        ];

        $client = new Client([
            'headers' => $headers
        ]);

        $r = $client->request($method, $url, [
            'body' => $body
        ]);
        return json_decode($r->getBody()->getContents(), true);
    }
}
?>
