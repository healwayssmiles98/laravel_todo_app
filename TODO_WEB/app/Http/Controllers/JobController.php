<?php

namespace App\Http\Controllers;

use App\Http\Common\Base\BaseController;
use App\Models\Job;
use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Http as HttpClient;
use App\Http\Common\ApisUrl\ApisUrl;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Common\Consts\Consts;
use App\Http\Common\Base\HttpTrait;
use Illuminate\View\View;

class JobController extends BaseController
{
    use HttpTrait;
    /**
     * token
     * @var string
     */
    private $token;

    public function token(Request $request) {
        Consts::$token = $request->input('token');
        //$this->token = $request->input('token');
        session()->push('access_token', $request->input('token'));
        session()->save();
        return \response()->json(Consts::$token);
    }

    /**
     * Display a listing of the resource.
     *;
     * @param Request $request
     * @return string
     */
    public function index(Request $request)
    {
        try {
            $page = 0;
            if ($request->has('page')) {
                $page = $request->input('page');
                //$pageSize = $request->input('pageSize');
                $apiUrl = ApisUrl::JOB_API . '?page=' . $page;
            }
            else {
                $apiUrl = ApisUrl::JOB_API;
            }
            $headers = [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . session()->get('access_token')[0]
            ];

            $client = new GuzzleClient([
                'headers' => $headers
            ]);

            $r = $client->request('GET', $apiUrl, [
                'body' => '{}'
            ]);
            $data = $r->getBody()->getContents();
            $response = json_decode($data, true);
            return view('job.index', compact('data', 'page', 'response'))->with(['success' => true]);
        }
        catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function create(Request $request)
    {
        try {
            $job = [
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'fk_job_type_id' => $request->input('type')
            ];

            $response = $this->httpClient(session()->get('access_token')[0], 'POST', ApisUrl::JOB_API, json_encode($job));
            return redirect('job')->with(
                [
                    'success' => true,
                    'data' => $response,
                    'errorCode' => 0,
                    'errorMessage' => ''
                ]
            );
        }
        catch (\Exception $ex) {
            return redirect('job')->with([
                'success' => false,
                'errorCode' => $ex->getCode(),
                'errorMessage' => $ex->getMessage(),
                'data' => []
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function show(int $id)
    {
        $types = $this->httpClient(session()->get('access_token')[0], 'GET', ApisUrl::JOB_TYPES_API);
        $response = $this->httpClient(session()->get('access_token')[0], 'GET', ApisUrl::JOB_API . $id);
        return view('job.edit', compact('response', 'types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Job $job
     * @return void
     */
    public function edit(Job $job)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Job $job
     * @param int $id
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, Job $job, int $id)
    {
        try {
            $job = [
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'start_date' => $request->input('start_date'),
                'end_date' => $request->input('end_date'),
                'fk_job_type_id' => $request->input('type')
            ];

            $response = $this->httpClient(session()->get('access_token')[0], 'PUT', ApisUrl::JOB_API . $id,
                json_encode(
                    ['job' => $job,
                        'request' => $request->all(),
                        'name' => $request->input('name'),
                        'description' => $request->input('description')
                    ]
                ));
            return redirect('job')->with(
                [
                    'success' => true,
                    'data' => $response,
                    'errorCode' => 0,
                    'errorMessage' => ''
                ]
            );
        }
        catch (\Exception $ex) {
            return redirect('job')->with(
                [
                    'success' => false,
                    'data' => [],
                    'errorCode' => $ex->getCode(),
                    'errorMessage' => $ex->getMessage()
                ]
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        try {
            $response = $this->httpClient(session()->get('access_token')[0], 'DELETE', ApisUrl::JOB_API . $id);
            return redirect()->route('job')->with(['response' => $response]);
        }
        catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function form () {
        try {
            $types = $this->httpClient(session()->get('access_token')[0], 'GET', ApisUrl::JOB_TYPES_API);
            return view('job.create', compact('types'));
        }
        catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
