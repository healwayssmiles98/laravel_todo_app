<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
      'name', 'description' , 'start_date', 'end_date', 'created_by', 'fk_job_type_id'
    ];
}
