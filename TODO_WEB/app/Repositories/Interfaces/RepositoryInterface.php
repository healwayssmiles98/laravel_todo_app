<?php
    namespace App\Repositories\Interfaces;
    interface RepositoryInterface {

        public function all($columns = array('*'));

        public function index();

        public function paginate(int $pageSize);

        public function create(array $data);

        public function update(array $data, $id);

        public function delete($id);

        public function find($id, $columns = array('*'));

        public function findBy($field, $value, $columns = array('*'));
    }

?>
