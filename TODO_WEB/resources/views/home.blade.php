@extends('layouts.app')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Chào mừng bạn !
                </div>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $(document).ready(function () {
           @if(session()->get('success') && session()->get('access_token'))
                localStorage.setItem('access_token', `{{ session('access_token')['access_token'] }}`);
                localStorage.setItem('expires_in', `{{ session('access_token')['expires_in'] }}`);
                localStorage.setItem('refresh_token', `{{ session('access_token')['refresh_token'] }}`);
            @endif

            // send token to server
            @if(!isset(session()->get('access_token')[0]))
                $.ajax({
                    beforeSend: function(xhr, type) {
                        if (!type.crossDomain) {
                            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                        }
                    },
                    url: '{{ route('token') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        token: `${localStorage.getItem('access_token')}`
                    },
                    success: function (result) {
                        console.log(result);
                    },
                    error: function (error) {
                        console.log(JSON.stringify(error));
                    }
                });
            @endif
        });
    </script>
@endsection
