@extends('layouts.app')
@section('content')
    @if (isset(session()->get('data')[0]))
        {{ session()->get('data')[0] }}
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Thêm mới công việc</div>
                        <div class="card-body">
                            <form method="post" action="{{ route('store') }}" id="formCreate">
                                @csrf
                                <div class="modal-header">
                                    <h4 class="modal-title">Thêm công việc</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Tên công việc</label>
                                        <input type="text" name="name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Mô tả</label>
                                        <input type="text" name="description" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Ngày bắt đầu</label>
                                        <input class="form-control" type="date" name="start_date" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>Ngày kết thúc</label>
                                        <input type="date" name="end_date" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Loại công việc</label>
                                        <select class="custom-control custom-select custom-select-sm" name="type" required>
                                                @for($i = 0; $i < count($types['data']); $i++)
                                                    <option value="{{ $types['data'][$i]['id'] }}">{{ $types['data'][$i]['job_type_name'] }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                   {{-- <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel"> --}}
                                    <input type="submit" class="btn btn-success" value="Thêm">
                                    {{--<button class="btn btn-primary close-form">Huỷ</button>--}}
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.close-form').click(function() {
               window.close();
            });

            if (`{{ session()->get('success') }}` == 1) {
                setTimeout(function () {
                  window.close();
                }, 500);
                $.notify("Thêm mới thành công", { position: 'top center', className: 'success' });
            }
        });
        /*$('#formCreate').submit(function (e) {
            $(this).submit();
            setTimeout(function () {
                window.close();
            }, 0);
        }); */
    </script>

@endsection()

