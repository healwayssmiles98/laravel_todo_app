@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Cập nhật công việc</div>
                    <div class="card-body">
                        <form action="{{ route('job_edit', $response['data']['id']) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="modal-header">
                                <h4 class="modal-title">Cập nhật công việc</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Tên công việc</label>
                                    <input type="text" name="name" class="form-control" value="{{ $response['data']['name'] }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Mô tả</label>
                                    <input type="text" name="description" class="form-control" value="{{ $response['data']['description'] }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Ngày bắt đầu</label>
                                    <input class="form-control" type="date" name="start_date" value="{{ $response['data']['start_date'] }}" required/>
                                </div>
                                <div class="form-group">
                                    <label>Ngày kết thúc</label>
                                    <input type="date" name="end_date" class="form-control" value="{{ $response['data']['end_date'] }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Loại công việc</label>
                                    <select class="custom-control custom-select custom-select-sm" name="type" required>
                                            @for($i = 0; $i < count($types['data']); $i++)
                                                @if ($types['data'][$i]['id'] == $response['data']['fk_job_type_id'])
                                                    <option value="{{ $types['data'][$i]['id'] }}" selected>{{ $types['data'][$i]['job_type_name'] }}</option>
                                                @else
                                                    <option value="{{ $types['data'][$i]['id'] }}">{{ $types['data'][$i]['job_type_name'] }}</option>
                                                @endif
                                            @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                {{-- <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel"> --}}
                                <input type="submit" class="btn btn-success" value="Sửa">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        /*$(window).on('beforeunload', function() {
            return confirm('Are you sure you want to leave?');
        });

        $(window).on('unload', function(){

        });*/

        $(document).ready(function () {
           let start_date = `{{ $response['data']['start_date'] }}`;
           let end_date = `{{ $response['data']['end_date'] }}`;
           $("[name=start_date]").val(start_date.split(' ')[0]);
           $("[name=end_date]").val(end_date.split(' ')[0]);

            /*if (`{{ session()->get('success') }}` == 1) {
                setTimeout(function () {
                    window.close();
                }, 500);
                $.notify("Thêm mới thành công", { position: 'top center', className: 'success' });
            }*/
        });
    </script>
@endsection()
