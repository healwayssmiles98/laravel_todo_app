@extends('layouts.app')
@section('content')

    <script type="text/javascript">
        $(document).ready(function() {
            // Activate tooltip
            $('[data-toggle="tooltip"]').tooltip();

            // Select/Deselect checkboxes
            let checkbox = $('table tbody input[type="checkbox"]');
            $("#selectAll").click(function(){
                if(this.checked){
                    checkbox.each(function(){
                        this.checked = true;
                    });
                } else{
                    checkbox.each(function(){
                        this.checked = false;
                    });
                }
            });
            checkbox.click(function(){
                if(!this.checked){
                    $("#selectAll").prop("checked", false);
                }
            });

        });
    </script>
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Manage <b>Jobs</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="{{ route('create') }}" class="btn btn-success create"><i class="material-icons">&#xE147;</i> <span>Thêm mới công việc</span></a>
                        {{-- <a href="#deleteEmployeeModal" class="btn btn-danger delete" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Xoá</span></a> --}}
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>
							<span class="custom-checkbox">
								<input type="checkbox" id="selectAll">
								<label for="selectAll"></label>
							</span>
                    </th>
                    <th>Tên công việc</th>
                    <th>Mô tả</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày kết thúc</th>
                    <th>Người tạo</th>
                    <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                @for($i = 0; $i < count($response['data']); $i ++)
                    <tr>
                        <td>
							<span class="custom-checkbox">
								<input type="checkbox" id="checkbox{{ $i }}" name="options[]" value="{{ $i }}">
								<label for="checkbox{{$i}}"></label>
							</span>
                        </td>
                        <td>{{ $response['data'][$i]['name'] }}</td>
                        <td>{{ $response['data'][$i]['description'] }}</td>
                        <td>{{ $response['data'][$i]['start_date'] }}</td>
                        <td>{{ $response['data'][$i]['end_date'] }}</td>
                        <td>{{ $response['data'][$i]['created_by'] }}</td>
                        <td>
                            <a href="{{ route('job_edit', $response['data'][$i]['id']) }}" link="{{ route('job_edit', $response['data'][$i]['id']) }}" class="edit" id="{{ $response['data'][$i]['id'] }}"><i class="material-icons" title="Edit">&#xE254;</i></a>
                            <a href="#deleteEmployeeModal" class="delete" link="{{ route('job_delete', $response['data'][$i]['id']) }}" id="{{ $response['data'][$i]['id'] }}" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                        </td>
                    </tr>
                @endfor
                </tbody>
            </table>
            <div class="clearfix">
                <div class="hint-text">Hiển thị <b>{{ $response['meta']['per_page'] }}</b> của <b>{{ $response['meta']['total'] }}</b> kết quả</div>
                <ul class="pagination">
                    <li class="page-item"><a href="{{ route('job') . "?page=1" }}">Đầu</a></li>
                    @for($i = $response['meta']['current_page']; $i < $response['meta']['last_page']; $i++)
                        <li class="page-item" page="{{ $i }}"><a href="{{ route('job') . "?page={$i}" }}" class="page-link">{{ $i }}</a></li>
                    @endfor
                    <li class="page-item"><a href="{{ route('job') . "?page={$response['meta']['last_page']}" }}">Cuối</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="editEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Employee</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-info" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Delete Modal HTML -->
    <div id="deleteEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="formDelete">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h4 class="modal-title">Xoá công việc</h4>
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p>Bạn có chắc muốn xoá công việc <b><span id="job-name" style="font-style: italic"></span></b> ?</p>
                        <p class="text-warning"><small>Xoá xong thì thôi, không khôi phục được.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Huỷ">
                        <input type="submit" class="btn btn-danger" value="Xoá">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {

             const url = window.location.href;
             const page = url.match(/page=\d+/);
             let windowCreate;
             let windowEdit;
             if (page == null) {
                 $("[page=1]").addClass("active");
             }
             else {
                 let pageNumber = page[0].split('=')[1];
                 $("[page=" + pageNumber + "]").addClass("active");
             }
            /*
            $(document).find('.create').click(function () {
                 windowCreate = window.open('{{ route('create') }}', 'create', 'height=800,width=800,left=' + screen.width/3 + ',top=' + screen.height/6);
            });
            $(document).find('.edit').click(function () {
                let link = $(this).attr('link');
                windowEdit = window.open(`${link}`, 'job_get', 'height=800,width=800,left=' + screen.width/3 + ',top=' + screen.height/6);
            }); */

            $(document).on('click', '.delete', function(){
                let link = $(this).attr('link');
                $('#formDelete').prop('action', `${link}`);
            });

            $('#formDelete').submit(function (e) {
                $('.close-modal').click();
                setTimeout(function() {
                    $(this).submit();
                }, 3000);
            });

            @if(session()->get('success'))
                if (`{{ session()->get('errorCode') }}` == 0) {
                    $.notify("Thực hiện thành công !", { position: 'top center', className: 'success', autoHideDelay: 2000 });
                    setTimeout(function () {
                        location.reload();
                    }, 2500);
                }
            @elseif(!session()->get('success'))
                //$.notify("Thực hiện thất bại, vui lòng thử lại !\nLỗi: " + `{{ session()->get('errorMessage') }}`, { position: 'top center', className: 'error' });
                {{ request()->session()->forget('errorCode') }}
            @endif
        });
    </script>

@endsection
