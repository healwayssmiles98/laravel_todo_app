<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http as HttpClient;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('job', 'JobController');


Route::get('todo/job/{id}', function ($id) {
    return HttpClient::get('http://localhost:8888/api/job/' . $id);;
})->name('getJob')->where(['id' => '[0-9]{1,}']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {

});

Route::middleware('auth')->group(function () {
    Route::get('job', 'JobController@index')->name('job');
    Route::get('job/{id}', 'JobController@show')->name('job_get');
    Route::put('job/{id}', 'JobController@update')->name('job_edit');
    Route::delete('job/{id}', 'JobController@destroy')->name('job_delete');
    Route::post('create', 'JobController@create')->name('store');
    Route::get('create', 'JobController@form')->name('create');
});

Route::post('token', 'JobController@token')->name('token');

Route::get('redis', function () {
   print_r(app()->make('redis'));
});
